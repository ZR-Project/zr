$(function(){
    $(".search-item a").click(function(){
        var parent = $(this).parent();
        parent.find("a").removeClass("on");
        $(this).addClass("on");
    });
    $(".nav-tabs li").click(function(){
        var name = $(this).find("a").html();
        $("#locationName").html(name);
    });
    loadTop(6);
});