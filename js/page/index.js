/**
 * Created by xiaoxiao on 2016/8/1.
 */
$(function(){
    $('#bannerCarousel').carousel({
        interval: 3000
    });
    $('#smallCarousel').carousel({
        interval: 3000
    });
    loadTop(1);
});