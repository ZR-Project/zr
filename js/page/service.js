//答案
$(document).on('click','.answer',function(){
    $(".answer").css("background-color","#fff")
    $(this).css("background-color","#f9f9f9")
    $(this).children(".answerDe").slideToggle()
})
//锚点变化
function showFlow(){
    var num = window.location.href.split('#')[1].split('=')[1];
    $(".titleTool span").eq(num).trigger('click');
    $("#load").show();
}
$(window).on("hashchange", function() {
    showFlow();
});
$(".turnTo").on('click',function(){
    $(".turnTo").removeClass("changeSize")
    $(".turnTo").siblings("img").hide()
    $(this).addClass("changeSize")
    $(this).siblings("img").show()
    $("#location").text($(this).text())
    window.location.href = window.location.href.split('#')[0]+'#num='+$(this).attr("data-index");
    var a="block/"
    a=a+$(this).attr("id")
    $("#load").load(a)
})
setTimeout(function(){showFlow();},50);
window.location.href = window.location.href +'#num=0';
loadTop(7);
