/**
 * Created by xiaoxiao on 2016/8/4.
 */
$(function(){
    $("#page-bottom").load("block/bottom.html");
});
/*加载头部文件*/
var loadTop =function(n){
    $("#page-top").load("block/top.html",function(){
        $.each($("#page-top .parent-nav li"),function(){
            if($(this).attr("data-id")==n){
                $(this).addClass("on");
            }
        });
        bindSubMenu();
    });
};
//二级菜单
var  bindSubMenu = function(){
    $(".fund-menu dl dt").mouseover(function(){
        $(".fund-menu dt").removeClass("on");
        $(".fund-menu dt").children(".redSquare").hide();
        $(this).children(".redSquare").show();
        $(this).addClass("on");
        $(".fund-list").removeClass("on");
        $("."+$(this).attr("data-target")).addClass("on");
    });
};